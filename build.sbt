import com.typesafe.sbt.SbtScalariform._

import scalariform.formatter.preferences._

name := """Esmeralda-WebApp"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

//resolvers += "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"
//resolvers += "Sonatype Staging" at "https://oss.sonatype.org/content/repositories/staging/"
resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += Resolver.jcenterRepo


lazy val root = (project in file(".")).enablePlugins(PlayScala)
routesGenerator := InjectedRoutesGenerator
routesImport += "utils.route.Binders._"

//play.Project.playScalaSettings
//ScalaJS javascript enablePlugins(ScalaJSPlugin)

libraryDependencies ++= Seq(
  // Play2-ReactiveMongo to your dependencies - only for Play 2.5.x
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.14", //"0.12-RC1", //"0.11.14",
  //"org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  // DB parse //"org.sorm-framework" % "sorm" % "0.3.19",
  // DB storage //"com.h2database" % "h2" % "1.4.192",
  //"org.scala-lang" % "scala-compiler" % scalaVersion.value force() // !!

  //scrimage => https://github.com/sksamuel/scrimage (licensed under the Apache 2)
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.0",
  //"com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.0",
  //"com.sksamuel.scrimage" %% "scrimage-filters" % "2.1.0"

  //"org.webjars.npm" % "typescript" % "2.0.0"

  "com.mohiva" %% "play-silhouette" % "4.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "4.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "4.0.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "4.0.0",
  "com.mohiva" %% "play-silhouette-testkit" % "4.0.0" % "test",
  "org.webjars" %% "webjars-play" % "2.5.0-2",
  "net.codingwell" %% "scala-guice" % "4.0.1",
  "com.iheart" %% "ficus" % "1.2.6",
  "com.typesafe.play" %% "play-mailer" % "5.0.0",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.5.0-akka-2.4.x",
  "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3",

  //jdbc,
  cache,
  ws,
  specs2 % Test,
  filters
)





//fork in run := true //For when using the activator.
//FILE project/play-fork-run.sbt.
// This plugin adds forked run capabilities to Play projects which is needed for Activator.
//addSbtPlugin("com.typesafe.play" % "sbt-fork-run-plugin" % "2.5.4")

scalacOptions ++= Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Xlint", // Enable recommended additional warnings.
  "-Ywarn-adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-nullary-override", // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
  "-Ywarn-numeric-widen" // Warn when numerics are widened.
)
