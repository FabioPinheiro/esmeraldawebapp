package utils

import play.Logger

/**
 * Created by fabio on 16-11-2016.
 */
trait MyLogger {
  /**
   * A named logger instance.
   */
  val logger = play.api.Logger(this.getClass)
  //val logger: Logger.ALogger = Logger.of(this.getClass.getCanonicalName)
  Logger.trace(s"START ${this.getClass.getCanonicalName} MyLogger")
  logger.info(s"START ${this.getClass.getCanonicalName}")
  def loggerTraceCall(method: String, args: Any*) =
    logger.trace(s"method call ${this.getClass.getCanonicalName}.$method(${args.map(_.toString).mkString(",")})")
}
