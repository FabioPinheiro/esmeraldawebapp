package utils

case class Params(var key: String = "", var params: Map[String, Seq[String]])
object Params {
  import play.api.mvc.QueryStringBindable

  implicit def deviceContextBinder(implicit stringBinder: QueryStringBindable[String]) =
    new QueryStringBindable[Params] {
      override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, Params]] = Some(Right(Params(key, params)))
      override def unbind(key: String, value: Params): String = ???
      // stringBinder.unbind()
    }
}