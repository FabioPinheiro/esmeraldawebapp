package filters

import javax.inject.Inject

import akka.stream.Materializer
import play.api.mvc._
import utils.MyLogger

import scala.concurrent.{ ExecutionContext, Future }

//https://www.clever-cloud.com/blog/engineering/2015/12/01/redirect-to-https-in-play/
class TLSFilter @Inject() (implicit override val mat: Materializer, exec: ExecutionContext) extends Filter with MyLogger {
  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    if (!requestHeader.secure) {
      logger.debug(s"TLSFilter: domain=${requestHeader.domain} ; host=${requestHeader.host} + uri=${requestHeader.uri}")
      Future.successful(Results.MovedPermanently("https://" + requestHeader.domain + requestHeader.uri))
    } else
      nextFilter(requestHeader).map(_.withHeaders("Strict-Transport-Security" -> "max-age=31536000; includeSubDomains"))
  }
}
