package actors

import java.io.{ File, InputStream }
import javax.inject._

import akka.actor._
import models.Mongo
import play.api.Configuration
import play.api.libs.json.JsObject
import reactivemongo.api.gridfs.DefaultFileToSave
import reactivemongo.bson.BSONObjectID

object DBActor {
  case object GetConfig
  case class SayHello(name: String)
  case class TODO() //TODO REMOVE
  case class AddUser(json: JsObject)
  case class AddProduct(json: JsObject)
  case class UploadProduct(json: JsObject)
  case class UploadMyHealthRecord(json: JsObject)
  case class AddImage(saveFile: DefaultFileToSave, inputStream: InputStream)
  case class GetUser(id: Option[String] = None, name: Option[String] = None)
  case class GetProduct(id: Option[String] = None, name: Option[String] = None)
  case class GetImage(id: Option[String] = None, name: Option[String] = None)
}

class DBActor @Inject() (val configuration: Configuration, val mongo: models.Mongo) extends Actor {
  import DBActor._
  val config = configuration.getString("my.config").getOrElse("none")

  def receive = { //TODO log
    case GetConfig =>
      sender() ! config
    case SayHello(name: String) =>
      sender() ! "Hello, " + name
    case TODO() =>
      println("Call DBActor TODO")
      sender() ! "Call DBActor TODO"

    case "gfs" =>
      sender() ! mongo.getGriFS
    case AddUser(json: JsObject) =>
      sender() ! mongo.addUser(json)
    case AddProduct(json: JsObject) =>
      sender() ! mongo.addProduct(json)
    case UploadProduct(json: JsObject) =>
      sender() ! mongo.uploadProduct(json)
    case UploadMyHealthRecord(json: JsObject) =>
      sender() ! mongo.uploadMyHealthRecord(json)
    case AddImage(saveFile: DefaultFileToSave, inputStream: InputStream) =>
      sender() ! mongo.addImage(saveFile, inputStream)
    case GetUser(id, name) =>
      sender() ! mongo.findUsers(query = Mongo.makeQuery(id = id, name = name))
    case GetProduct(id, name) =>
      sender() ! mongo.findProducts(query = Mongo.makeQuery(id = id, name = name))
    //case GetImage(id,name) =>
    //  sender() ! mongo.findImage(filter=Mongo.makeFilter(id=id,name=name))
  }
}
