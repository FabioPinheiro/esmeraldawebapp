module test {
  class Startup {
    public static main(): number {
      console.log("Hello World - The FP.js is build from TypeScript")
      return 0;
    }
  }
  export let aux1 = { "type": "google", name: "this is a Image", "uri": "URI" } as fmgp.str.ImageInterface;
  export let itemTest = { "_id": "D", "name": "D", "model": "MODEL", "description": "DESCRIPTION", "category": "CATEGORY", "images": [aux1] };
  export let itemTest2 = '{"_id": "D","name": "D","model": "MODEL","description": "DESCRIPTION","category": "CATEGORY","images": [{"type": "google","uri": "URI", "new":"works?"}]}'
  export let p: fmgp.str.Product = itemTest;
  export let p2: fmgp.str.Product = JSON.parse(itemTest2) as fmgp.str.Product;
}
module debug {
  export let lastXHR: XMLHttpRequest;
  export let lastXHRonSuccess: (XMLHttpRequest) => any
}
let xxx1;
let xxx2;
let xxx3;

//TODO !
//FIXME string as any as SomeType  very unsafe
//FIXME xrh responseType

console.trace = function() {}; //FIXME

module fmgp {

  //API START
  export module routes {
    export let productURL: string = "/models/product";
    export function formData2URIComponents(formData): string {
      let ret = [];
      formData.forEach(function (value, key) {
        ret.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
      }); return ret.join("&");
    } //encodeURI()
  }
  export function getModels(contentUrl: string, appendTo: string, printFunction: (data: any) => HTMLDivElement, clearAppendTo: boolean = false) {
    function call(xhr: XMLHttpRequest) {
      let items: JSON[] = JSON.parse(xhr.responseText);
      let aux = document.getElementById(appendTo);
      if (clearAppendTo) aux.innerHTML = "";
      items.forEach(function (item) { aux.appendChild(printFunction(item)) })
    }
    xhr.xHttpCall(contentUrl, xhr.MethodType.Get, xhr.ResponseType.Text, call, null)
  }
  export module state {
    export function init() {
      console.log("FMGP INIT! - This FP.js is build from TypeScript");
      user = new str.User("fabio", "Fabio", true);
      admin = user.admin;
    }
    export let user = null;
    export let admin: Boolean = false;
    export function reloadPage() { console.log("RELOAD!"); } //location.reload()
  }
  //API END


  export declare module str {
    export const enum ImageType { Google, Mongo, Link } //TODO use

    export interface ID { _id: string } //FIXME $oid
    export interface UserInterface extends ID { name?: string; admin?: Boolean }
    export interface Product extends ID {
      name: string; model: string;
      price?: string; //FIXME flout
      description?: string;
      size?: string;
      color?: string;
      form?: string;
      category?: string;
      theme?: string;
      images?: ImageInterface[];
    }
    export interface ImageInterface { type: string; name?: string; }
    export interface ImageGoogle extends ImageInterface { uri: string; }
    export interface ImageMongo extends ImageInterface { _id: Object; }
    export interface ImageLink extends ImageInterface { link: string; }
  }

  export module utils {
    //export function appendHtml(el: HTMLElement, e: Node) { for (var i = 1; i < arguments.length; i++) { el.appendChild(arguments[i]); }}
    export function divRemoveChild(div: HTMLDivElement) { while (div.firstChild) { div.removeChild(div.firstChild); } }

    //export function generateIMG(item: str.Image) { //FIXME HTMLElement
    //  return `<img src='/models/image?id='${str.imageSrc(item)}' alt='${item.name}' max-height='150px' max-width='150px'>` as any as HTMLImageElement //FIXME str.imageSrc(item)
    //}
    export function generateTextArea(data: JSON): HTMLTextAreaElement {
      const textarea = document.createElement("TextArea") as HTMLTextAreaElement;
      textarea.name = 'json'; textarea.rows = 20;
      textarea.appendChild(document.createTextNode(JSON.stringify(data, null, 2)))  //FIXME refactoring 4
      return textarea
    }
  }

  export module str {
    export class User implements str.UserInterface {
      public constructor(public _id: string, public name: string, public admin?: boolean) { }
    }
    export class Image implements str.ImageInterface {
      public json: JSON;
      public _id: string;
      public type: string;
      public name?: string;
      //constructor(_id: string, type: string, name?: string);
      constructor(obj: str.ImageInterface);
      constructor(obj?: any) { this.json = obj; for (let prop in obj) this[prop] = obj[prop]};

      public imageSrc(): string { //TODO SRT function
        if (this.type == "mongo") { return "/models/image?id=" + (<ImageMongo>this)._id; }//FIXME TODO BUG .$oid;
        else if (this.type == "google") { return "https://lh3.googleusercontent.com/" + (<ImageGoogle>(this as any)).uri; }
        else if (this.type == "link") { return (<ImageLink>(this as any)).link; }
        else if (this.type == "none") { return "/assets/images/missing-file.png"; }
        else { console.error("fmgp.str.Image.imageSrc(): error Image with unknown type:" + this.type); return "/assets/images/missing-file.png";}
      }
    } //formJson = function (obj: JSON) { return new Image(obj['_id'], obj['type'], obj['name']) }//for (var prop in obj) this[prop] = obj[prop];
    export module Product {
      export function log(p: str.Product) { console.log(`Printing Product "${p.name}" with id:"${p._id}"`); }

      export function editableHTML(p: fmgp.str.Product): HTMLFormElement {
        const formElement = document.createElement("FORM") as HTMLFormElement;
        formElement.onsubmit = function (e) { e.preventDefault(); };
        formElement.appendChild(fmgp.utils.generateTextArea(p as any as JSON));

        const uploadBUTTON = document.createElement("BUTTON") as HTMLButtonElement;
        uploadBUTTON.onclick = function () {
          if (confirm("Do you want to upload this document?") == true) {
            xhr.xHttpCall(`${routes.productURL}?id=${p._id}`, xhr.MethodType.Post, xhr.ResponseType.Text, xhr.xhrShowResponseText, new FormData(formElement));
            state.reloadPage();
          }
        };
        uploadBUTTON.appendChild(document.createTextNode("UPLOAD"));
        formElement.appendChild(uploadBUTTON);

        const deleteBUTTON = document.createElement("BUTTON") as HTMLButtonElement;
        deleteBUTTON.onclick = function () {
          const ret = confirm("Attention! Do you really want to DELTE this document?");
          if (ret == true) {
            xhr.xHttpCallDelete(`${fmgp.routes.productURL}?id=${p._id}`);
            state.reloadPage();
          }
        }
        deleteBUTTON.appendChild(document.createTextNode("DELETE"))
        formElement.appendChild(deleteBUTTON)

        return formElement
      }


      export function displayWindowLayout(item: fmgp.str.Product, div = <HTMLDivElement>document.getElementById("idDisplayContent")) { //FIXME refactoring idDisplayContent
        utils.divRemoveChild(div);
        if (state.admin == true) { div.appendChild(fmgp.str.Product.editableHTML(item)); }
        else { div.innerHTML = `<p>You are not in admin mode</p>` }
      }

      export function displayLayout(product: str.Product): HTMLDivElement {
        function openProductOn(id) { fmgp.str.Product.displayWindowLayout(storage.getID(id)); document.getElementById("idDisplay").style.display = 'block'; }

        storage.saveID(product);
        const id = product._id;
        const div = document.createElement("div");
        div.className = "fp-flex-item fp-item fp-show-hover"; //div.setAttribute("class", "fp-item fp-show-hover");

        const img = document.createElement("img");
        console.trace("displayLayout: " + product.images[0]);
        img.src = new str.Image(product.images[0]).imageSrc(); // str.Image.prototype.imageSrc() //str.imageSrc(product.images[0])
        img.alt = product.name;
        img.onclick = function (e) { console.log(id); openProductOn(id) }; //REMOVE log

        const title = document.createElement("p");
        title.innerHTML = "<b>" + product.name + "</b>";
        const category = document.createElement("p");
        category.innerHTML = `<span>Categories: ${product.category}</span><br><span>Preço: ${product.price}€</span><br>`

        const hidendDiv = document.createElement("div");
        hidendDiv.className = "fp-hide";
        hidendDiv.innerHTML = ""
          + `<span>Ref: ${product._id}</span><br>`
          + `<span>Tamanho: ${product.size}</span><br>`
          + `<span>Cor: ${product.color}</span><br>`
          + `<span>Form: ${product.form}</span><br>`
          + `<span>Tema: ${product.theme}</span><br>`
          + `<p>${product.description}</p>`;

        div.appendChild(img); //div.insertAdjacentElement('beforeend',img)
        div.appendChild(title);
        div.appendChild(category);
        div.appendChild(hidendDiv);
        return div
      }
    }
  }

  export module storage {
    export function idToString(id) {
      if (id.constructor == Object) { return id.$oid; }
      else if (id.constructor == String) { return id; }
      else { return console.error("idToString fail"); }
    }

    export function saveID(item: str.ID) {
      if (typeof (Storage) !== "undefined") {//localStorage/sessionStorage
        const idAux = idToString(item._id);
        if (idAux != null) { sessionStorage.setItem(idAux, JSON.stringify(item)); } //localStorage
        else { throw new Error("ID is null on saveID") }
      } else { throw new Error("Sorry! No Web Storage support..") }
    }

    export function getID(id: string) {
      if (typeof (Storage) !== "undefined") {//localStorage/sessionStorage
        const idAux = idToString(id);
        if (idAux != null) { return JSON.parse(sessionStorage.getItem(idAux)); }
        else { throw new Error(" ID is null on getID") }
      } else { throw new Error("Sorry! No Web Storage support..") }
    }

    export function listID(callback: (any) => any = console.log) {
      for (var i = 0; i < sessionStorage.length; i++) {
        callback(sessionStorage.key(i));
      }
    }
  }

  export module xhr {


    export const enum MethodType { Get, Post, Delete }
    export const enum ResponseType { Text }
    export const enum ContentType { None, Json, UrlEncoded, FormData }
    export function MethodType2String(m: MethodType): string {
      switch (m) {
        case MethodType.Get: return 'GET';
        case MethodType.Post: return 'POST';
        case MethodType.Delete: return 'DELETE';
        default: throw Error('unknown MethodType:' + m);
      }
    }
    export function ResponseType2String(r: ResponseType): string {
      switch (r) {
        case ResponseType.Text: return 'text';
        default: throw Error('unknown ResponseType:' + r);
      }
    }
    export function ContentType2SetXHR(c: ContentType, xhr: XMLHttpRequest): void {
      switch (c) {
        case ContentType.None: 'None'; break;
        case ContentType.Json: xhr.setRequestHeader('Content-Type', 'application/json'); break;
        case ContentType.UrlEncoded: xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); break;
        case ContentType.FormData: xhr.setRequestHeader('Content-Type', 'multipart/form-data'); break;
        default: throw Error('unknown ContentType:' + c);
      }
    }

    export function xhrShowResponseText(xhr: XMLHttpRequest) {
      console.log("xhrShowResponseText" + xhr.responseText);
      alert(xhr.responseText);
    } //JSON.parse(xhr.responseText)

    function formToKV(x) {
      const kvFormData = new FormData();  //var kv = {};
      for (let i = 0; i < x.length; i++) {
        const key = x.elements[i].name; const value = x.elements[i].value;
        if (x.elements[i].disabled === false && key.length !== 0 && value.length !== 0)
        { kvFormData.append(key, value) }  //kv[key] = value;
      } return kvFormData;  //return JSON.stringify(kv)
    }

    export function xHttpCallDelete(url) { xHttpCall(url, MethodType.Delete, ResponseType.Text, xhrShowResponseText, null) }


    export function xHttpCall(url, callType: MethodType, responseType: ResponseType, onSuccess, data: FormData = null, contentType: ContentType = ContentType.None, asynchronous: boolean = true) {
      if (url == null || url == undefined)
        throw new Error("fmgp.xhr.xHttpCall: the url is null");
      //https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/responseType
      const xhr = new XMLHttpRequest();
      xhr.open(MethodType2String(callType), url, asynchronous); // true for asynchronous
      xhr.responseType = ResponseType2String(responseType);
      xhr.onreadystatechange = function () { //use-me instead of 'xhr.onload'
        if (xhr.readyState == 4) {
          debug.lastXHR = xhr;
          debug.lastXHRonSuccess = onSuccess;
          if (xhr.status == 200 || xhr.status === 201 || xhr.status === 202) { onSuccess(xhr); }//callback(xmlHttp.responseText);
          else if (xhr.status === 400 || xhr.status == 500) {
            const msg = "xHttpCall FAIL! " + xhr.status + "(" + xhr.statusText + "): " + xhr.response;
            document.body.innerHTML = xhr.response;
            throw new Error(msg);
          } else { throw new Error("xHttpCall FAIL " + xhr.status + "(" + xhr.statusText + "): " + xhr.response); }
        }
      };
      ContentType2SetXHR(contentType, xhr);
      xhr.send(data); //xhr.send(encodeURI('operation=' + 'test') + '&' + encodeURI('operation2=' + 'test2'))
      return xhr
    }

    function postCommand(form, url = '/models/command', onSuccess = xhrShowResponseText, responseType = ResponseType.Text) { //REMOVE
      event.preventDefault();
      if (typeof form == 'string') {
        form = document.forms[form]; //document.getElementById("name")
      }
      if (form.reportValidity()) {
        const kvFormData = formToKV(form);
        //var texxt = "Sending this FormData:\n"
        //for (let element of kvFormData.entries()) { texxt = texxt + element[0] + " -> " + element[1] + "\n"}
        xHttpCall(url, MethodType.Post, responseType, onSuccess, kvFormData);
        return true
      } else { return false }
    }
  }
}

fmgp.state.init()



module trash { //REMOVE
  export function printJson(item: any) {
    let line = "-> ";
    for (let key in item) { if (item.hasOwnProperty(key)) { line = line + (key + ':' + item[key]) + "; " } }
    const li = document.createElement('li');
    return li.textContent = line + "."
  }
  //NOTUSED function printUser(user) {var li = document.createElement("li"); li.innerHTML = user["name"] + "; "; return li;}

  export function jsonSyntaxHighlight(json) {
    //http://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript
    if (typeof json != 'string') {
      json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
      let cls = 'number';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'key';
        } else {
          cls = 'string';
        }
      } else if (/true|false/.test(match)) {
        cls = 'boolean';
      } else if (/null/.test(match)) {
        cls = 'null';
      }
      return '<span class="' + cls + '">' + match + '</span>';
    });
  }
}