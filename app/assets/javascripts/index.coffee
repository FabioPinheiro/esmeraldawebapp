#printObjKeys = (obj) -> for key, value of obj
#  "#{key}=#{value}"
###
#$ ->
#  $.get "/models/user", (users) ->
#    $.each users, (index,user) ->
#      $("#modelsData-user").append $("<li>").text "user: " + printObjKeys(user).join("; ")


#$ ->
#  $.get "/models/product", (products) ->
#    $.each products, (index,product) ->
#      $("#modelsData-product").append $("<li>").text "product: " + printObjKeys(product).join("; ")
###

#showContent2 = (products) ->
#    $.each products, (index,product) ->
#      $("#modelsData-content").append $("<li>").text "product: " + printObjKeys(product).join("; ")