package controllers

import java.io.FileInputStream
import javax.inject.{ Inject, Singleton, _ }

import actors.DBActor
import akka.actor.ActorRef
import akka.pattern.ask
import akka.stream.Materializer
import akka.util.{ ByteString, Timeout }
import com.fasterxml.jackson.core.JsonParseException
import models.Mongo
import play.Logger
import play.api.http.HttpEntity
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.mvc._
import reactivemongo.api.gridfs._
import reactivemongo.bson.BSONDocument
import reactivemongo.play.json.BSONFormats.BSONDocumentFormat
import utils.{ MyLogger, Params }

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.parsing.json.JSONObject

@Singleton
class ModelsController @Inject() (@Named("db-actor") dbActorRef: ActorRef, mongo: Mongo)(implicit val mat: Materializer) extends Controller with MyLogger {
  implicit val timeout: Timeout = 10.seconds
  val CONTENT_DISPOSITION_ATTACHMENT = "attachment"
  val CONTENT_DISPOSITION_INLINE = "inline"

  def filters(filters: Params) = Action {
    loggerTraceCall("filters" + filters)
    import reactivemongo.play.json.BSONFormats.toJSON
    val ret = toJSON(Mongo.makeQuery(filters))
    logger.debug("filters return:" + ret)
    Ok(s"filters:\n$filters \n$ret")
  }

  def index = Action {
    loggerTraceCall("index")
    Ok(views.html.modelshtml.main())
  }

  def indexUsers = Action {
    loggerTraceCall("indexUsers")
    Ok(views.html.modelshtml.aux("user"))
  }

  def indexProducts = Action {
    loggerTraceCall("indexProducts")
    Ok(views.html.modelshtml.aux("product"))
  }

  def indexImage(id: Option[String]) = Action {
    loggerTraceCall("indexImage")
    Ok(views.html.images(id.getOrElse("")))
  }

  def getUser(id: Option[String], name: Option[String]) = Action.async {
    loggerTraceCall("getUser", id, name)
    mongo.findUsers(query = Mongo.makeQuery(id = id, name = name)).map(Ok(_))
  }
  //(dbActorRef ? DBActor.GetUser(id=id, name=name)).mapTo[Future[JsArray]].flatMap(identity).map{Ok(_)}

  def getProduct(id: Option[String], name: Option[String], brand: Option[String], theme: Option[String], size: Option[String]) = Action.async {
    loggerTraceCall("getProduct", id, name, theme, size)
    mongo.findProducts(query = Mongo.makeQuery(("brand" -> brand), ("theme" -> theme), ("size" -> size))).map(Ok(_))
  }
  //(dbActorRef ? DBActor.GetProduct(id=id, name=name)).mapTo[Future[JsArray]].flatMap(identity).map{Ok(_)}

  def getImageFile(id: Option[String], name: Option[String]) = Action.async {
    loggerTraceCall("getImageFile", id, name)
    mongo.findImagesFile(query = Mongo.makeQuery(id = id, name = name)).map(Ok(_))
  }
  //(dbActorRef ? DBActor.GetProduct(id=id, name=name)).mapTo[Future[JsArray]].flatMap(identity).map{Ok(_)}

  def getImage(id: Option[String], name: Option[String]) = Action.async { //Some("57b7629c790000970031fd4f")
    loggerTraceCall("getImage", id, name)
    //val aux = (dbActorRef ? DBActor.GetImage(id=id, name=name)).mapTo[Future[(HttpEntity, String, String, Long)]]
    mongo.findImage(query = Mongo.makeQuery(id = id, name = name)).flatMap { e =>
      val (futureData, fileName: String, contentType: String, fileSize: Long) = e
      futureData.map { imageDta =>
        //FIXME val httpEntity2 = HttpEntity.Streamed(new ByteArrayInputStream(image.bytes),None,Some(contentType))
        val httpEntity = HttpEntity.Strict(ByteString(imageDta), Some(contentType))
        Result(header = ResponseHeader(OK), body = httpEntity).as(contentType).withHeaders(
          //CONTENT_LENGTH -> fileSize.toString,
          CONTENT_DISPOSITION -> (s"""$CONTENT_DISPOSITION_INLINE; filename=someName; filename*=UTF-8''"""
            + java.net.URLEncoder.encode(fileName, "UTF-8").replace("+", "%20"))
        )
      }
    } recover { case e => Logger.warn(s"NoContent on getImage call (with id=$id): ", e); NoContent }
  }

  def insertImage = Action.async { implicit request =>
    loggerTraceCall("insertImage")
    request.body.asMultipartFormData match {
      case Some(data) =>
        val fileDescription = data.dataParts.get("description").map(_.mkString(" "))
        data.files.find(_.key == "image") match {
          case Some(files) =>
            mongo.addImage(files.filename, files.contentType, fileDescription, files.ref.file).map { id =>
              println("id: " + id.stringify)
              Ok(views.html.images(id.stringify))
            }
          case None => Future(BadRequest("No file for 'image' key"))
        }
      case None => Future(BadRequest("No data"))
    }
  }

  def uploadProduct = Action.async { implicit request =>
    loggerTraceCall("uploadProduct")
    val keyValueMap = request.body.asMultipartFormData.get.dataParts //FIXME NoSuchElementException: None.get
    println(keyValueMap)
    keyValueMap.get("json") match {
      case None =>
        Future(BadRequest(s"uploadProduct: no json?!\n" + request.headers))
      case Some(Seq(jsonString)) =>
        try {
          val json = Json.parse(jsonString).as[JsObject]: JsObject //val bson = reactivemongo.play.json.BSONFormats.toBSON(json).get
          (dbActorRef ? DBActor.UploadProduct(json)).map(e => Accepted("Update Done: " + e)) //FIXME REMOVE  DBActor.UploadProduct
        } catch {
          case e: JsonParseException =>
            logger.error("uploadProduct: JsonParseException", e)
            Future(BadRequest(s"uploadImageFile: JsonParseException!\n" + request.headers))
        }
    }
  }

  def uploadImageFile = Action.async { implicit request =>
    loggerTraceCall("uploadImageFile")
    val keyValueMap = request.body.asMultipartFormData.get.dataParts
    println(keyValueMap)
    keyValueMap.get("json") match {
      case None =>
        Future(BadRequest(s"uploadImageFile: no json?!\n" + request.headers))
      case Some(Seq(jsonString)) =>
        try {
          val json = Json.parse(jsonString).as[JsObject]: JsObject
          mongo.uploadImageFile(json).map(e => Accepted("Update Done: " + e))
        } catch {
          case e: JsonParseException =>
            logger.error("uploadImageFile: JsonParseException", e)
            Future(BadRequest(s"uploadImageFile: JsonParseException!\n" + request.headers))
        }
    }
  }

  def deleteProduct(id: String) = Action.async { implicit request =>
    loggerTraceCall("deleteProduct")
    mongo.deleteProduct(id).map(s => Ok(s))
  }
  def deleteImage(id: String) = Action.async { implicit request =>
    loggerTraceCall("deleteImage")
    Future(NotImplemented(s"deleteImage id=$id")) //TODO
  }
  def deleteImageFile(id: String) = Action.async { implicit request =>
    loggerTraceCall("deleteImageFile")
    Future(NotImplemented(s"deleteImageFile id=$id")) //TODO
  }

  def command = Action.async { implicit request => //asMultipartFormData
    loggerTraceCall("command")
    println("-------------------------- command --------------------------")
    val method = request.method
    val query = request.queryString
    val keyValueHeadersMap = request.headers.toMap
    var keyValueMap = Map[String, Seq[String]]() //TODO convert into JsValue and add to var json
    request.body.asFormUrlEncoded.foreach(e => keyValueMap ++= e)
    request.body.asMultipartFormData.foreach(e => keyValueMap ++= e.dataParts)
    val files = request.body.asMultipartFormData.map(_.files)
    val json: Option[JsValue] = request.body.asJson

    //printAnyContentType(request.body)
    println("info--------------------------")
    println(method)
    println(query)
    println(keyValueHeadersMap)
    println(keyValueMap)
    println(files)

    //FIXME should I Remove? kv2 kv3?
    def kv2json(kv: Map[String, Seq[String]]): JsObject = {
      val kv2 = kv.mapValues(v => v.mkString(" ")) //transform ArrayBuffer[String] => String
      val kv3 = kv2.filterNot(p => p._2.isEmpty) //remove empty fields
      val jsonObject = JSONObject(kv3).toString() //util.parsing.json
      Json.parse(jsonObject).as[JsObject]: JsObject
    }

    keyValueMap.get("operation") match {
      case None =>
        Future(BadRequest(s"No operation key for method= $method\nquery=$query\nkeyValueMap=$keyValueMap  : \n" + request.headers))
      case Some(Seq(operation)) =>
        operation match {
          case "test" =>
            Future(Ok(s"This is a test. method= $method\nquery=$query\nkeyValueMap=${keyValueMap}"))
          case "insert" =>
            val cKey = keyValueMap.get("collection")
            val json = kv2json(keyValueMap.filterKeys(!List("operation", "collection").contains(_)))
            cKey match {
              case Some(Seq("user")) =>
                (dbActorRef ? DBActor.AddUser(json)).map(_ => Ok("Insert Done"))
              case Some(Seq("product")) =>
                (dbActorRef ? DBActor.AddProduct(json)).map(_ => Ok("Insert Done"))
              case Some(Seq("image")) =>
                val mtdFile = files.flatMap(_.find(_.key == "image")).get //: Option[(File, Option[String])] .get
                val (fileKey, fileData, fileContentType, filename) = (mtdFile.key, mtdFile.ref.file, mtdFile.contentType, mtdFile.filename)
                val (name, description) = (keyValueMap.get("name").head, keyValueMap.get("description").map(_.mkString(" ")))
                println("file: File  " + fileData)
                println("file: contentType  " + fileContentType)
                println("file: fileKey  " + fileKey)
                val metadata = BSONDocument("name" -> name, "description" -> description)
                val saveFile = DefaultFileToSave(filename = Some(filename), contentType = fileContentType, metadata = metadata)
                val stream = new FileInputStream(fileData)
                (dbActorRef ? DBActor.AddImage(saveFile, stream)).map(_ => Ok("Insert Done"))
            }
          case "show" =>
            val cKey = keyValueMap.get("collection")
            //val json = kv2json(keyValueMap.filterKeys(! List("operation", "show").contains(_)))
            cKey match {
              case Some(Seq("user")) => (dbActorRef ? DBActor.GetUser(None, None)).
                mapTo[Future[JsArray]].flatMap(identity).map { Ok(_) }
              case Some(Seq("product")) => (dbActorRef ? DBActor.GetProduct(None, None)).
                mapTo[Future[JsArray]].flatMap(identity).map { Ok(_) }
              case Some(Seq("image")) =>
                val aux = (dbActorRef ? DBActor.GetImage(Some("57b7629c790000970031fd4f"), None)).mapTo[Future[(HttpEntity, String, String, Long)]]
                aux.flatMap(identity).map { e => //(gfsEnt:HttpEntity,fileName:String,contentType:String,fileSize:Long)
                  val (gfsEnt: HttpEntity, fileName: String, contentType: String, fileSize: Long) = e
                  //val gfsEnt = e._1
                  //val fileName = e._2
                  //val contentType = e._3
                  //val fileSize = e._4
                  println(s"$contentType\n${gfsEnt}") //MongoConnection.serve
                  Result(
                    header = ResponseHeader(OK), body = gfsEnt
                  ).as(contentType).
                    withHeaders(
                      CONTENT_LENGTH -> fileSize.toString,
                      CONTENT_DISPOSITION -> (s"""$CONTENT_DISPOSITION_INLINE; filename=someName; filename*=UTF-8''"""
                        + java.net.URLEncoder.encode(fileName, "UTF-8").replace("+", "%20"))
                    )
                }
              case any =>
                println("Fail show any: " + any)
                Future(Json.parse(""""Fail show any"""")).map(BadRequest(_))
            } //TODO use on f.onComplete(): f.onFailure() + f.onSuccess()
          //for { //if I call "collections-map" it still run in parallel right? Yes
          //  results <- fff//Future.sequence(fff)
          //} yield Ok(results) //FIXME!!
          case any =>
            val error = s"Fail command operation:'$any'"; println(error); Future(BadRequest(error))
        }
      case _ => val error = "WTH?!"; println(error); Future(BadRequest(error))
    }
  }
}
