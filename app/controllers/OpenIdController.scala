package controllers

import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.openid._

class OpenIdController @Inject() (openIdClient: OpenIdClient) extends Controller {
  /*
  def login = Action {Ok(views.html.login())}

  def loginPost = Action.async { implicit request =>
    Form(single("openid" -> nonEmptyText)).bindFromRequest.fold({ error =>
      Logger.info(s"bad request ${error.toString}")
      Future.successful(BadRequest(error.toString))
    }, { openId =>
      openIdClient.redirectURL(openId, routes.OpenIdController.openIdCallback.absoluteURL())
        .map(url => Redirect(url))
        .recover { case t: Throwable => Redirect(routes.OpenIdController.login)}
    })
  }

  def openIdCallback = Action.async { implicit request =>
    openIdClient.verifiedId(request).map(info => Ok(info.id + "\n" + info.attributes))
      .recover {
        case t: Throwable =>
          // Here you should look at the error, and give feedback to the user
          Redirect(routes.OpenIdController.login)
      }
  }
  */
}