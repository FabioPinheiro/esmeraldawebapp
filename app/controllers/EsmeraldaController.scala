package controllers

import javax.inject._

import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.impl.providers.SocialProviderRegistry
import models.User
import models.services.{ AuthTokenService, UserService }
import play.api.i18n.{ I18nSupport, MessagesApi }
import play.api.mvc.{ Action, RequestHeader }
import utils.MyLogger
import utils.auth.DefaultEnv

object Pages extends Enumeration {
  type Pages = Value
  val Home, Shop, Contact, Admin, None, Msg = Value

  def fromString(s: String): Pages = {
    s match { //Pages.withName()
      case "" | "home" => Pages.Home
      case "shop" => Pages.Shop
      case "contact" => Pages.Contact
      case "admin" => Pages.Admin
      case "msg" => Pages.Msg
      case _ => Pages.None
    }
  }
}

@Singleton
class EsmeraldaController @Inject() ( //val messagesApi: MessagesApi,
    socialProviderRegistry: SocialProviderRegistry,
    silhouette: Silhouette[DefaultEnv],
    userService: UserService,
    authTokenService: AuthTokenService,
    implicit val webJarAssets: WebJarAssets,
    implicit val messagesApi: MessagesApi
) extends MainController with I18nSupport with MyLogger { //with I18nSupport

  private lazy val mainPage = views.html.pages.page("home") //It need only be generated once
  private lazy val aboutPage = views.html.pages.page("about") //It need only be generated once
  private lazy val shopPage = views.html.pages.page("shop") //It need only be generated once
  private lazy val contactPage = views.html.pages.page("contact") //It need only be generated once
  //private def infoPage(user: User) = views.html.pages.page("user", Some(user))
  //private def adminPage(user: User)(implicit request: RequestHeader) =

  override def index = Action {
    loggerTraceCall("index Redirect to home")
    Redirect("home")
  }
  def home = Action {
    loggerTraceCall("home")
    Ok(mainPage)
  }
  def about = Action {
    loggerTraceCall("about")
    Ok(aboutPage)
  }
  def shop = Action {
    loggerTraceCall("shop")
    Ok(shopPage)
  }
  def contact = Action {
    loggerTraceCall("contact")
    Ok(contactPage)
  }
  def admin = silhouette.SecuredAction { implicit request =>
    loggerTraceCall("admin")
    Ok(views.html.pages.admin(request.identity))
  }
  def user = silhouette.SecuredAction { request =>
    loggerTraceCall("info")
    Ok(views.html.pages.page("user", Some(request.identity)))
  }

}
