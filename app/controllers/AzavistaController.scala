package controllers

import javax.inject._

import play.api.mvc._

import scala.concurrent.ExecutionContext

@Singleton
class AzavistaController @Inject() ()(implicit exec: ExecutionContext) extends Controller {
  def index = Action { Ok(views.html.azavista()) }
}
