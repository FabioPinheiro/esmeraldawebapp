package controllers

import javax.inject._

import utils.MyLogger
import play.api.mvc._
import play.twirl.api.Html

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class MainController @Inject() extends Controller with MyLogger {

  def index = Action {
    loggerTraceCall("index")
    Ok(views.html.main("MainController")(Html("Some Content")))
  }
  def welcome() = Action {
    loggerTraceCall("welcome")
    Redirect("@documentation")
  }
  def hello(name: String, n: Option[Int]) = Action {
    loggerTraceCall("hello")
    Ok("Hello " + name + "!" + n)
  }

  def headers = List( //FIXME HACK
    "Access-Control-Allow-Origin" -> "*",
    "Access-Control-Allow-Methods" -> "GET, POST, OPTIONS, DELETE, PUT",
    "Access-Control-Max-Age" -> "3600",
    "Access-Control-Allow-Headers" -> "Origin, Content-Type, Accept, Authorization",
    "Access-Control-Allow-Credentials" -> "true"
  )
  def options = Action { request => //FIXME HACK
    NoContent.withHeaders(headers: _*)
  }
}
