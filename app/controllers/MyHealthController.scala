package controllers

import javax.inject.{ Inject, Named }

import actors.DBActor
import akka.actor.ActorRef
import akka.pattern.ask
import akka.stream.Materializer
import akka.util.Timeout
import com.fasterxml.jackson.core.JsonParseException
import models.Mongo
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{ JsObject, Json }
import play.api.mvc.{ Action, Controller }
import utils.MyLogger

import scala.concurrent.Future
import scala.concurrent.duration._

class MyHealthController @Inject() (@Named("db-actor") dbActorRef: ActorRef, mongo: Mongo)(implicit val mat: Materializer) extends Controller with MyLogger {
  implicit val timeout: Timeout = 10.seconds

  def getRecord(id: Option[String], name: Option[String]) = Action.async {
    loggerTraceCall("getRecord", id, name)
    mongo.findMyHealthRecord(query = Mongo.makeQuery(id, name)).map(Ok(_))
  }

  def uploadRecord = Action.async { implicit request =>
    loggerTraceCall("uploadRecord")
    val keyValueMap = request.body.asMultipartFormData.get.dataParts //FIXME NoSuchElementException: None.get
    println(keyValueMap)
    keyValueMap.get("json") match {
      case None =>
        Future(BadRequest(s"uploadMyHealthRecord: no json?!\n" + request.headers))
      case Some(Seq(jsonString)) =>
        try {
          val json = Json.parse(jsonString).as[JsObject]: JsObject //val bson = reactivemongo.play.json.BSONFormats.toBSON(json).get
          (dbActorRef ? DBActor.UploadMyHealthRecord(json)).map(e => Accepted("Update Done: " + e)) //FIXME REMOVE  DBActor.UploadMyHealthRecord
        } catch {
          case e: JsonParseException =>
            logger.error("uploadMyHealthRecord: JsonParseException", e)
            Future(BadRequest(s"uploadImageFile: JsonParseException!\n" + request.headers))
        }
    }
  }

  def deleteRecord(id: String) = Action.async { implicit request =>
    loggerTraceCall("deleteRecord", id)
    mongo.deleteMyHealthRecord(id).map(s => Ok(s))
  }
}
