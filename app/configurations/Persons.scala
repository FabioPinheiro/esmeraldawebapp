package configurations

object WebSiteOwner extends Persons.Esmeralda
object WebSiteDesigner extends Persons.Fabio

object Persons {
  trait Person {
    type User = Option[String]
    val BitbucketUser: User
    val FacebookUser: User
    val TwitterUser: User
    val LinkedInUser: User
    val DefaultUser = "NONE" //FIXME change to ""

    def Name: String
    def Email: String
    def Bitbucket = "https://bitbucket.org/" + BitbucketUser.getOrElse(DefaultUser)
    def Facebook = "https://www.facebook.com/" + FacebookUser.getOrElse(DefaultUser)
    def Twitter = "https://twitter.com/" + TwitterUser.getOrElse(DefaultUser)
    def LinkedIn = "https://www.linkedin.com/in/" + LinkedInUser.getOrElse(DefaultUser)
  }

  trait Esmeralda extends Person {
    override val Name = "Esmeralda Pinheiro"
    override val Email = "esmeralda.pinheiro@gmail.com"
    override val BitbucketUser = None
    override val FacebookUser = Some("esmeralda.pinheiro1")
    override val TwitterUser = None
    override val LinkedInUser = Some("esmeralda-pinheiro-8a300430")
  }

  trait Fabio extends Person {
    override val Name = "Fabio Pinheiro"
    override val Email = "fabiomgpinheiro@gmail.com"
    override val BitbucketUser = Some("FabioPinheiro")
    override val FacebookUser = Some("fabiomgpinheiro")
    override val TwitterUser = None
    override val LinkedInUser = Some("fabio-pinheiro")
  }

}