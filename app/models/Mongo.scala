package models

import java.io.{ FileInputStream, InputStream }
import javax.inject.Inject

import akka.stream.Materializer
import akka.stream.scaladsl.{ Sink, Source }
import com.sksamuel.scrimage.Image
import play.api.libs.json.{ JsObject, _ }
import play.api.libs.streams.Streams
import play.modules.reactivemongo.{ MongoController, ReactiveMongoApi, ReactiveMongoComponents }
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.gridfs.{ DefaultFileToSave, GridFS }
import reactivemongo.api.{ BSONSerializationPack, Cursor, ReadPreference }
import reactivemongo.bson.{ BSONArray, BSONDocument, BSONObjectID, BSONValue, Producer }
import reactivemongo.play.json._
import utils.{ MyLogger, Params }

import scala.concurrent.{ ExecutionContext, Future }

object Mongo {
  val userCollectionName = "users"
  val productCollectionName = "products"
  val myHealthRecordCollectionName = "myhealthrecord"
  val imageParCollectionName = "img"
  val imageFilesCollectionName = "img.files"

  val defaultQueryAny: BSONDocument = BSONDocument() //JsObject = Json.obj()
  val defaultQueryNone: BSONDocument = BSONDocument("_id" -> BSONDocument("$exists" -> false))
  val defaultProjection: JsObject = Json.obj() //Json.obj("_id"->0)
  def makeQuery(id: Option[String] = None, name: Option[String] = None) =
    BSONDocument("_id" -> id.map(BSONObjectID(_)), "name" -> name) //Json.obj("_id" -> id.map(BSONObjectID(_)), "name" -> name)
  def makeQuery(agrs: Producer[(String, BSONValue)]*) = //HOTFIX see ModelsController.getProduct
    BSONDocument(agrs: _*)
  def makeQuery(agrs: Params) =
    BSONDocument("$and" -> BSONArray(agrs.params.map { case (k, values) => BSONDocument("$or" -> BSONArray(values.map(v => BSONDocument(k -> v)))) }))

}

class Mongo @Inject() (val reactiveMongoApi: ReactiveMongoApi) extends MongoController with ReactiveMongoComponents with MyLogger {
  import ExecutionContext.Implicits.global
  implicit val reader = reactivemongo.api.gridfs.Implicits.DefaultReadFileReader

  protected def fUserCollection = {
    loggerTraceCall("fUserCollection")
    reactiveMongoApi.database.map(_.collection[BSONCollection](Mongo.userCollectionName))
  }
  protected def fProductCollection = {
    loggerTraceCall("fProductCollection")
    reactiveMongoApi.database.map(_.collection[BSONCollection](Mongo.productCollectionName))
  }
  protected def fMyHealthRecordCollection = {
    loggerTraceCall("fMyHealthRecordCollection")
    reactiveMongoApi.database.map(_.collection[BSONCollection](Mongo.myHealthRecordCollectionName))
  }
  protected def fImageGridFS = {
    //FIXME [JSONSerializationPack.type] or?  [BSONSerializationPack.type]
    loggerTraceCall("fImageGridFS")
    reactiveMongoApi.database.map(db => GridFS[BSONSerializationPack.type](db, Mongo.imageParCollectionName))
  }
  protected def fImageFileCollection = {
    loggerTraceCall("fImageFileCollection")
    reactiveMongoApi.database.map(_.collection[BSONCollection](Mongo.imageFilesCollectionName))
  }

  def getGriFS = fImageGridFS

  def find()(implicit ec: ExecutionContext): Future[List[JsObject]] =
    fUserCollection.flatMap(_.find(Json.obj()).cursor[JsObject](ReadPreference.Primary).collect[List]())

  //val t= Await.result(db, Duration.Inf) //bolck!!

  def addUser(item: JsObject): Future[Unit] = {
    loggerTraceCall("addUser", item)
    fUserCollection.flatMap(_.insert(item).map(_ => {}))
  }
  def addProduct(item: JsObject): Future[Unit] = {
    loggerTraceCall("addProduct", item)
    fProductCollection.flatMap(_.insert(item).map(_ => {}))
  }
  def addImage(saveFile: DefaultFileToSave, inputStream: InputStream) = {
    loggerTraceCall("addImage", saveFile, "'inputStream")
    fImageGridFS.flatMap { gridFS =>
      gridFS.writeFromInputStream(saveFile, inputStream).map {
        readFile => readFile.id.asInstanceOf[BSONObjectID] // println(s"id =${id.stringify}")
      }
    }
  } //TODO Please note that you should really consider reading [[http://www.mongodb.org/display/DOCS/Indexes]]
  def addImage(fileName: String, contentType: Option[String], fileDescription: Option[String], file: java.io.File) = {
    loggerTraceCall("addImage", fileName, contentType, fileDescription, "'file")
    val metadata = BSONDocument("originalName" -> fileName, "description" -> fileDescription,
      "imageEditing" -> BSONDocument("max" -> BSONDocument("maxW" -> 200, "maxH" -> 300)))
    val saveFile = DefaultFileToSave(filename = Some(fileName), contentType = contentType, metadata = metadata)
    val stream = new FileInputStream(file)
    val image = Image.fromStream(stream).max(maxW = 200, maxH = 300)
    fImageGridFS.flatMap { gridFS =>
      gridFS.writeFromInputStream(saveFile, image.stream).map {
        readFile => readFile.id.asInstanceOf[BSONObjectID] // println(s"id =${id.stringify}")
      }
    }
  }

  def uploadProduct(item: JsObject): Future[Unit] = {
    loggerTraceCall("uploadProduct", item)
    fProductCollection.flatMap { e =>
      val selector = item.value.get("_id") match {
        case Some(id) => BSONDocument("_id" -> id)
        case None => Mongo.defaultQueryNone //make a new item
      }
      val modifier = e.updateModifier(item, upsert = true)
      e.findAndModify(selector, modifier).map(e => println("uploadProduct: " + e))
    }
  }
  def uploadMyHealthRecord(item: JsObject): Future[Unit] = {
    loggerTraceCall("uploadMyHealthRecord", item)
    fMyHealthRecordCollection.flatMap { e =>
      val selector = item.value.get("_id") match {
        case Some(id) => BSONDocument("_id" -> id)
        case None => Mongo.defaultQueryNone //make a new item
      }
      val modifier = e.updateModifier(item, upsert = true)
      e.findAndModify(selector, modifier).map(e => println("uploadMyHealthRecord: " + e))
    }
  }
  def uploadImageFile(item: JsObject): Future[Unit] = {
    loggerTraceCall("uploadImageFile", item)
    fImageFileCollection.flatMap(e => {
      val selector = BSONDocument("_id" -> item.value.get("_id").get) //IT must have
      val modifier = e.updateModifier(item, upsert = true)
      e.findAndModify(selector, modifier).map(e => println("uploadProduct: " + e))
    })
  }

  def findUsers(query: BSONDocument = Mongo.defaultQueryAny, projection: JsObject = Mongo.defaultProjection): Future[JsArray] = {
    loggerTraceCall("findUsers", query, projection)
    fUserCollection.flatMap { collection =>
      val cursor: Cursor[JsObject] = collection.find(query).projection(projection).cursor[JsObject]()
      val futurePersonsList: Future[List[JsObject]] = cursor.collect[List]() // gather all the JsObjects in a list
      futurePersonsList.map { JsArray(_) } // transform the list into a JsArray
    }
  }
  def findProducts(query: BSONDocument = Mongo.defaultQueryAny, projection: JsObject = Mongo.defaultProjection): Future[JsArray] = {
    loggerTraceCall("findProducts", query, projection)
    fProductCollection.flatMap { collection =>
      val cursor: Cursor[JsObject] = collection.find(query).projection(projection).cursor[JsObject]()
      val futurePersonsList: Future[List[JsObject]] = cursor.collect[List]() // gather all the JsObjects in a list
      futurePersonsList.map { JsArray(_) } // transform the list into a JsArray
    }
  }
  def findMyHealthRecord(query: BSONDocument = Mongo.defaultQueryAny, projection: JsObject = Mongo.defaultProjection): Future[JsArray] = {
    loggerTraceCall("findMyHealthRecord", query, projection)
    fMyHealthRecordCollection.flatMap { collection =>
      val cursor: Cursor[JsObject] = collection.find(query).projection(projection).cursor[JsObject]()
      val futurePersonsList: Future[List[JsObject]] = cursor.collect[List]() // gather all the JsObjects in a list
      futurePersonsList.map { JsArray(_) } // transform the list into a JsArray
    }
  }
  def findImagesFile(query: BSONDocument = Mongo.defaultQueryAny, projection: JsObject = Mongo.defaultProjection): Future[JsArray] = {
    loggerTraceCall("findImagesFile", query, projection)
    fImageFileCollection.flatMap { collection =>
      val cursor: Cursor[JsObject] = collection.find(query).projection(projection).cursor[JsObject]()
      val futurePersonsList: Future[List[JsObject]] = cursor.collect[List]() // gather all the JsObjects in a list
      futurePersonsList.map { JsArray(_) } // transform the list into a JsArray
    }
  }

  def findImage(query: BSONDocument = Mongo.defaultQueryAny)(implicit mat: Materializer) = {
    loggerTraceCall("findImage", query)
    //FIXME (filter:JsObject = Mongo.defaultFilter, projection:JsObject = Mongo.defaultProjection)
    //val fileOutputStream = new FileOutputStream("/home/fabio/workspace/FabioWeb/tmpTest.png")
    fImageGridFS.flatMap { gridFS =>
      gridFS.find(query).headOption.filter(_.isDefined).map(e => e.get).map { file =>
        @inline def gfsPub = Streams.enumeratorToPublisher(gridFS.enumerate(file))
        //@inline def bytesString = Source.fromPublisher(gfsPub).map{ bytes =>ByteString.fromArray(bytes)}
        // try {//throw new RuntimeException("FIXME printStackTrace")} catch { case e:Throwable => println("findImage try catch" + e.printStackTrace())}

        //val chunks3 = Source.fromPublisher(gfsPub).runReduce((a,b)=>a++b).map(ByteString.fromArray(_))
        val sink = Sink.fold[Array[Byte], Array[Byte]](Array())((a, b) => a ++ b)
        val futureData: Future[Array[Byte]] = Source.fromPublisher(gfsPub).runWith(sink)
        //val inputStream:InputStream = Source.fromPublisher(gfsPub).map(ByteString.fromArray(_))
        //  .runWith(StreamConverters.asInputStream(FiniteDuration(5, TimeUnit.SECONDS)))

        val contentType = file.contentType.getOrElse("application/octet-stream")
        (futureData, file.filename.get, contentType, file.length)
      }
    }
  }

  def deleteMyHealthRecord(id: String): Future[String] = {
    loggerTraceCall("deleteMyHealthRecord", id)
    fMyHealthRecordCollection.flatMap { e =>
      val selector = BSONDocument("_id" -> id)
      e.remove(selector, firstMatchOnly = true).map { e =>
        logger.info(s"deleteMyHealthRecord remove($id): ${e.message}")
        e.message
      }
    }
  }

  def deleteProduct(id: String): Future[String] = {
    loggerTraceCall("deleteProduct", id)
    fProductCollection.flatMap { e =>
      val selector = BSONDocument("_id" -> id)
      e.remove(selector, firstMatchOnly = true).map { e =>
        logger.info(s"deleteProduct remove($id): ${e.message}")
        e.message
      }
    }
  }

  /*duplicated plz use findImagesFile
  def imagesList(query:BSONDocument = Mongo.defaultQuery)(implicit mat: Materializer) = {
    //FIXME (filter:JsObject = Mongo.defaultFilter, projection:JsObject = Mongo.defaultProjection)
    //val fileOutputStream = new FileOutputStream("/home/fabio/workspace/FabioWeb/tmpTest.png")
    fImageGridFS.flatMap { gridFS =>
      gridFS.find(query).collect[List]().map{ xxx =>
        JsArray(xxx.map { file =>
          val jID = JsObject(Seq(("_id",BSONFormats.toJSON(file.id))))
          //println(s"imagesList: ${file.filename} - ${file.metadata.elements}")
          //file.metadata.elements.foreach((x) => println(s"key:${x._1}: ${x._2}"))
          file.metadata.elements.map { case (k:String,v:BSONValue) =>
            JsObject(Seq[(String,JsValue)]((k,BSONFormats.toJSON(v))))
            //BSONFormats.BSONValueReads.writes(v).as[JsObject]))
          }.foldLeft(z=jID)((z,j:JsObject) => z.++(j))
        })
      }
    }
  }*/
}

