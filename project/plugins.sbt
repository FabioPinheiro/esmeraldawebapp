//logLevel := Level.Warn
logLevel in compile := Level.Debug
// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.9")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0")

// The Sass compilation plugin
addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.6")

// web plugins
//addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.0")
//addSbtPlugin("com.typesafe.sbt" % "sbt-jshint" % "1.0.4")
//addSbtPlugin("com.typesafe.sbt" % "sbt-rjs" % "1.0.8")
//addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.1")
addSbtPlugin("com.typesafe.sbt" % "sbt-mocha" % "1.1.0")
addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.6")

// javascript
//addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.13")
// coffeescript
//addSbtPlugin("com.typesafe.sbt" % "sbt-coffeescript" % "1.0.0")
// typescript
addSbtPlugin("name.de-vries" % "sbt-typescript" % "0.3.0-beta.4")

//docker
//addSbtPlugin("com.spotify" % "docker-client" % "5.0.2")
