# This is the main configuration file for the application.
# https://www.playframework.com/documentation/latest/ConfigFile

#http.port=disabled
#https.port=9443
#play.server.https.keyStore.path=/home/fabio/.keystore
#play.server.https.keyStore.password=

## IDE
play.editor="http://localhost:63342/api/file/?file=%s&line=%s"

# Registers the request handler
# ~~~~~
play.http.requestHandler = "play.api.http.DefaultHttpRequestHandler"

# Registers the filters
# ~~~~~
play.http.filters = "filters.Filters"
libraryDependencies += filters


# The application DI modules
# ~~~~~
play.modules.enabled += "modules.Module"
play.modules.enabled += "modules.BaseModule"
#play.modules.enabled += "modules.JobModule"
play.modules.enabled += "modules.SilhouetteModule"
#play.modules.enabled += "play.api.libs.mailer.MailerModule"
play.modules.disabled += "com.mohiva.play.silhouette.api.actions.SecuredErrorHandlerModule"
play.modules.disabled += "com.mohiva.play.silhouette.api.actions.UnsecuredErrorHandlerModule"
play.modules.enabled += "play.modules.reactivemongo.ReactiveMongoModule"

mongodb.uri = "mongodb://localhost:27018/esmeraldadb"


include "application.secret.conf"

## Secret key
play.crypto.secret = ${?PLAY_SECRET}
## Mongo
mongodb.uri = ${?MONGODB_URI}

include "silhouette.conf"

mongo-async-driver.akka.loglevel = "WARNING"
# Akka config
akka {
  loglevel = "INFO"
  jvm-exit-on-fatal-error=off

  # Auth token cleaner
  quartz.schedules.AuthTokenCleaner {
    expression = "0 0 */1 * * ?"
    timezone = "UTC"
    description = "cleanup the auth tokens on every hour"
  }
}

# Play mailer
#play.mailer {
#  host = "smtp.sendgrid.net"
#  port = 587
#  tls = true
#  user = "" FIX ME
#  user = ${?SENDGRID_USERNAME}
#  password = "" FIX ME
#  password = ${?SENDGRID_PASSWORD}
#}

# Security Filter Configuration - Content Security Policy
play.filters.headers {
  contentSecurityPolicy = "default-src 'self';"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" img-src 'self' *.fbcdn.net *.twimg.com *.googleusercontent.com *.xingassets.com vk.com *.yimg.com secure.gravatar.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" style-src 'self' 'unsafe-inline' cdnjs.cloudflare.com maxcdn.bootstrapcdn.com cdn.jsdelivr.net fonts.googleapis.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" font-src 'self' fonts.gstatic.com fonts.googleapis.com cdnjs.cloudflare.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" script-src 'self' clef.io cdnjs.cloudflare.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" connect-src 'self' twitter.com *.xing.com;"
  contentSecurityPolicy = ${play.filters.headers.contentSecurityPolicy}" frame-src clef.io;"
}

## Internationalisation
play.i18n.langs = [ "en" ]

## Play HTTP settings
# ~~~~~
play.http {
  ## Router
  # https://www.playframework.com/documentation/latest/JavaRouting
  # https://www.playframework.com/documentation/latest/ScalaRouting
  # ~~~~~
  # Define the Router object to use for this application.
  # This router will be looked up first when the application is starting up,
  # so make sure this is the entry point.
  # Furthermore, it's assumed your route file is named properly.
  # So for an application router like `my.application.Router`,
  # you may need to define a router file `conf/my.application.routes`.
  # Default to Routes in the root package (aka "apps" folder) (and conf/routes)
  #router = my.application.Router

  ## Action Creator
  # https://www.playframework.com/documentation/latest/JavaActionCreator
  # ~~~~~
  #actionCreator = null

  ## ErrorHandler
  # https://www.playframework.com/documentation/latest/JavaRouting
  # https://www.playframework.com/documentation/latest/ScalaRouting
  # ~~~~~
  # If null, will attempt to load a class called ErrorHandler in the root package,
  #errorHandler = null

  ## Filters
  # https://www.playframework.com/documentation/latest/ScalaHttpFilters
  # https://www.playframework.com/documentation/latest/JavaHttpFilters
  # ~~~~~
  # Filters run code on every request. They can be used to perform
  # common logic for all your actions, e.g. adding common headers.
  # Defaults to "Filters" in the root package (aka "apps" folder)
  # Alternatively you can explicitly register a class here.
  #filters = my.application.Filters

  ## Session & Flash
  # https://www.playframework.com/documentation/latest/JavaSessionFlash
  # https://www.playframework.com/documentation/latest/ScalaSessionFlash
  # ~~~~~
  session {
    # Sets the cookie to be sent only over HTTPS.
    #secure = true

    # Sets the cookie to be accessed only by the server.
    #httpOnly = true

    # Sets the max-age field of the cookie to 5 minutes.
    # NOTE: this only sets when the browser will discard the cookie. Play will consider any
    # cookie value with a valid signature to be a valid session forever. To implement a server side session timeout,
    # you need to put a timestamp in the session and check it at regular intervals to possibly expire it.
    #maxAge = 300

    # Sets the domain on the session cookie.
    #domain = "example.com"
  }

  flash {
    # Sets the cookie to be sent only over HTTPS.
    #secure = true

    # Sets the cookie to be accessed only by the server.
    #httpOnly = true
  }
}

## Netty Provider
# https://www.playframework.com/documentation/latest/SettingsNetty
# ~~~~~
play.server.netty {
  # Whether the Netty wire should be logged
  #log.wire = true

  # If you run Play on Linux, you can use Netty's native socket transport
  # for higher performance with less garbage.
  #transport = "native"
}

## Filters
# https://www.playframework.com/documentation/latest/Filters
#https://www.playframework.com/documentation/2.5.x/SecurityHeaders#Configuring-Security-Headers
#https://www.playframework.com/documentation/2.5.x/CorsFilter

# libraryDependencies += filters
play.filters {
  ## CORS filter configuration
  # https://www.playframework.com/documentation/latest/CorsFilter
  # ~~~~~
  # CORS is a protocol that allows web applications to make requests from the browser
  # across different domains.
  # NOTE: You MUST apply the CORS configuration before the CSRF filter, as CSRF has
  # dependencies on CORS settings.
  cors {
    # Filter paths by a whitelist of path prefixes
    pathPrefixes = ["/"]# ["/some/path", ...]

    # The allowed origins. If null, all origins are allowed.
    allowedOrigins = null #FIXME ["http://.example.com"] #https://www.playframework.com/documentation/2.5.x/AllowedHostsFilter#configuring-allowed-hosts

    # The allowed HTTP methods. If null, all methods are allowed
    allowedHttpMethods = null #["GET", "POST", "DELETE", "OPTIONS", "DELETE"]

    # The allowed HTTP headers. If null, all headers are allowed.
    allowedHttpHeaders = null #["Accept"]

    # The exposed headers - set custom HTTP headers to be exposed in the response (by default no headers are exposed)
    #exposedHeaders = ["Access-Control-Allow-Origin","Access-Control-Allow-Methods","Access-Control-Allow-Headers"]

    # Whether to support credentials
    #supportsCredentials = true

    # The maximum amount of time the CORS meta data should be cached by the client - 
    # set how long the results of a preflight request can be cached in a preflight result cache (by default 1 hour)
    preflightMaxAge = 3 days
  }

  ## CSRF Filter
  # https://www.playframework.com/documentation/latest/ScalaCsrf#Applying-a-global-CSRF-filter
  # https://www.playframework.com/documentation/latest/JavaCsrf#Applying-a-global-CSRF-filter
  # ~~~~~
  # Play supports multiple methods for verifying that a request is not a CSRF request.
  # The primary mechanism is a CSRF token. This token gets placed either in the query string
  # or body of every form submitted, and also gets placed in the users session.
  # Play then verifies that both tokens are present and match.
  csrf {
    # Sets the cookie to be sent only over HTTPS
    #cookie.secure = true

    # Defaults to CSRFErrorHandler in the root package.
    #errorHandler = MyCSRFErrorHandler
  }

  ## Security headers filter configuration
  # https://www.playframework.com/documentation/latest/SecurityHeaders
  # ~~~~~
  # Defines security headers that prevent XSS attacks.
  # If enabled, then all options are set to the below configuration by default:
  headers {
    # The X-Frame-Options header. If null, the header is not set.
    frameOptions = null #"DENY" FIXME

    # The X-XSS-Protection header. If null, the header is not set.
    xssProtection = null #"1; mode=block" FIXME

    # The X-Content-Type-Options header. If null, the header is not set.
    contentTypeOptions = null #"nosniff" FIXME

    # The X-Permitted-Cross-Domain-Policies header. If null, the header is not set.
    permittedCrossDomainPolicies = null #"master-only" FIXME

    # The Content-Security-Policy header. If null, the header is not set.
    contentSecurityPolicy = null #"default-src 'self'" FIXME
  }

  ## Allowed hosts filter configuration
  # https://www.playframework.com/documentation/latest/AllowedHostsFilter
  # ~~~~~
  # Play provides a filter that lets you configure which hosts can access your application.
  # This is useful to prevent cache poisoning attacks.
  hosts {
    # Allow requests to example.com, its subdomains, and localhost:9000.
    #allowed = [".example.com", "localhost:9000"]
  }
}

## Evolutions
# https://www.playframework.com/documentation/latest/Evolutions
# ~~~~~
# Evolutions allows database scripts to be automatically run on startup in dev mode
# for database migrations. You must enable this by adding to build.sbt:
#
# libraryDependencies += evolutions
#
play.evolutions {
  # You can disable evolutions for a specific datasource if necessary
  #db.default.enabled = false
}
